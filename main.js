/*
Ant / gas molecule  abilities:
1. move = Acceleration
2. communicate with other ants = trails

goal:
find and eat food the fastest
explore the fastest
*/

window.onload = function () 
{
	init();
};

//canvas vars
var canvas;
var context;

//ant vars
var Ants = [];
var Asize = 10;
var Aacc = 1;

//Food var
var Foods = [];
var Fsize = 20;

//trail var
var Trails = [];
var Tsize = 10;
var prevX;
var prevY;
var coloringIn;

//grid size
var GridSize;
var Grid;
var homeX;
var homeY;

//id for interval
var ID;

//bool vars and steps
var verbose = false;
var steps = 0;
var DrawA = true;

//point
var point = 
{
	x : 0,
	y : 0
};

//ant class
function Ant( x, y, foundFood, foundTrail, steps, Trails, madeTrail, c )
{
	this.x = x;
	this.y = y;
	this.homeX = x;
	this.homeY = y;
	this.foundFood = foundFood;
	this.foundTrail = foundTrail;
	this.steps = steps;
	this.Trails = Trails;
	this.madeTrail = madeTrail;
	this.c = c;
}

//trail class
function Trail(x, y, prevX, prevY, antIndex)
{
	this.x = x;
	this.y = y;
	this.prevX = prevX;
	this.prevY = prevY;
	this.antIndex = antIndex;
}

//food class
function Food( x0, y0, x1, y1, x2, y2, x3, y3, Amt)
{
	this.x0 = x0;
	this.y0 = y0;
	this.x1 = x1;
	this.y1 = y1;
	this.x2 = x2;
	this.y2 = y2;
	this.x3 = x3;
	this.y3 = y3;
	this.Amt = Amt;
}

//init function that reads file or displays deault
function init()
{
	if (verbose) console.log("init");

	canvas = document.getElementById("canvas");
	context = canvas.getContext("2d");

	GridSize = canvas.width/Asize;
	Grid = Create2DArray(GridSize);

	//if (verbose) console.log("Grid[0][0]: " + Grid[0][0] );

	// homeX = Math.round(Math.random()*GridSize);
	// homeY = Math.round(Math.random()*GridSize);
	// coloringIn = rgb((Math.round(Math.random()*256)),(Math.round(Math.random()*256)),(Math.round(Math.random()*256)));

	if (verbose) console.log("GridSize: " + GridSize + "by" + GridSize );

	//add Ants to Antarray
	for (var i = 0; i < 100 ; i++)
	{
		homeX = Math.round(Math.random()*GridSize);
		homeY = Math.round(Math.random()*GridSize);
		coloringIn = rgb( (Math.round(Math.random()*255)), 
						  (Math.round(Math.random()*255)), 
						  (Math.round(Math.random()*255)) 
						);
		// x, y, foundFood, foundTrail, steps, Trails, madeTrail, color
		Ants.push( new Ant( homeX, 
							homeY,//Math.round(Math.random()*GridSize), 
							false, false, 0, [],false, coloringIn
						   ) 
				 );
	}

	//add food to grid
	// x0, y0, x1, y1, x2, y2, x3, y3, Amt
	for (var i = 0; i < 5; i++)
	{
		var Fx = Math.round(Math.random()*(GridSize-2)), 
			Fy = Math.round(Math.random()*(GridSize-2)),//GridSize/2 - 1;
			f = new Food( Fx, Fy, Fx + 1, Fy, Fx, Fy + 1, Fx + 1, Fy + 1, 100 );

		Foods.push( f );

		Grid[f.y0][f.x0] = f;
		Grid[f.y1][f.x1] = f;
		Grid[f.y2][f.x2] = f;
		Grid[f.y3][f.x3] = f;
	}
	
	//if (verbose) console.log("Food x y " + Foods[0].x + " " + Foods[0].y);	

	//home
	if (verbose) console.log("home x y " + homeX + " " + homeY);

	//draw init screen
	draw();
	start();
}

function move()
{

}

function communicate()
{

}

function retrieve()
{

}

//where ants make there move, foundFood booling changes, leave trail
function update()
{
	steps++;

	if (Foods.length === 0) stop();

	for(var antz in Ants)
	{
		var i = antz;

		antz = Ants[antz];
		//if (antz.foundFood && antz.x == antz.homeX && antz.y == antz.homeY) continue;

		var moveX = Math.round(Math.random());
		switch(Math.round(Math.random()))
		{
			case 0: 
				moveX *= 1;
				break;
			case 1: 
				moveX *= -1;
				break;
		}

		var moveY = Math.round(Math.random());
		switch(Math.round(Math.random()))
		{
			case 0: 
				moveY *= 1;
				break;
			case 1: 
				moveY *= -1;
				break;
		}

		if (antz.foundTrail === true && antz.foundFood !== true)
		{
			//If on trail looking for food
			//if (verbose) console.log("followTrail");

			var spot = Grid[antz.y][antz.x];
			
			if ( spot instanceof Trail )
			{
				//follow trail
				antz.x = spot.prevX;
				antz.y = spot.prevY;
			}
			else if ( spot instanceof Food )
			{
				//found food go home
				//if (verbose) console.log("followTrail foundFood!!! " + spot.Amt);
				antz.foundFood = true;
				antz.foundTrail = false;
				spot.Amt--;	
			}
			else if ( spot === undefined )
			{
				if (verbose) console.log("stuck");

				for (var T in antz.Trails) 
			    {
					T = antz.Trails[T];
					// console.log( Grid[T.y][T.x] );
					Grid[T.y][T.x] = undefined;
			    }
				antz.Trails.splice(0, antz.Trails.length);
				antz.foundTrail = false;
				antz.madeTrail = false;

			}
		}
		else if ( isLegal( antz.x + moveX, antz.y + moveY) )
		{
			if (!antz.foundFood)
			{
				if ( isTrail( antz.x + moveX, antz.y + moveY ) )
				{
					//found trail now need to follow it
					//if (verbose)console.log("foundTrail");
					antz.x += moveX;
					antz.y += moveY;
					antz.foundTrail = true;
				}
				else if ( isFood( antz.x + moveX, antz.y + moveY ) )
				{
					//found food now start making trail home and take a piece of food
					if (verbose) console.log("isFood");
					antz.foundFood = true;
					prevX = antz.x;
					prevY = antz.y;
					antz.Trails.push(new Trail(antz.x, antz.y, antz.x + moveX, antz.y + moveY, i));
					Grid[antz.y][antz.x] = antz.Trails[antz.Trails.length-1];
					antz.steps = steps;

					Grid[antz.y + moveY][antz.x + moveX].Amt--;
				}
				else
				{
					//if all else false make a pseudo-random move
					antz.x += moveX;
					antz.y += moveY;
				}
			}
			else
			{
				if (antz.x != antz.homeX || antz.y != antz.homeY)
				{
					//take shortest path home, maybe change this to follow unique trail home
					//if (verbose) console.log("ET phone home");
					var slopeX = antz.homeX - antz.x;
					var slopeY = antz.homeY - antz.y;
					prevX = antz.x;
					prevY = antz.y;

					if (slopeX !== 0) antz.x += (slopeX > 0 ? 1 : -1);
					if (slopeY !== 0) antz.y += (slopeY > 0 ? 1 : -1);
					
					if (!antz.madeTrail && !(Grid[antz.y][antz.x] instanceof Food) )
					{
						antz.Trails.push(new Trail(antz.x, antz.y, prevX, prevY, i));
						Grid[antz.y][antz.x] = antz.Trails[antz.Trails.length-1];
					}
				}
				else
				{
					//home now follow food again
					//if (verbose) console.log("Winner " + i + " took " + antz.steps + " ant steps and world steps " + steps);
					antz.foundFood = false;
					antz.foundTrail = false;
					antz.madeTrail = true;
				}
			}
		}
	}
	draw();
}

//draws function for "animation"
function draw()
{
	//console.log("DRAW");
	context.clearRect(0, 0, canvas.width, canvas.height);

	for (var antz in Ants) 
	{
		antz = Ants[antz];

	    for (var t in antz.Trails) 
	    {
			//if (Ants[Trails[t].antIndex].foundFood == false) {Trails.splice(t,1); continue;};
			t = antz.Trails[t];

			context.fillStyle = antz.c !== undefined ? antz.c : null;

			// if (verbose)console.log( antz.c );

			context.fillRect(t.x * Tsize, t.y * Tsize, Tsize, Tsize);
	    }
	}

    for (var antz in Ants) 
	{
		antz = Ants[antz];

	    if(DrawA)
	    {
			context.fillStyle = "black";

			context.fillRect(antz.x * Asize, antz.y * Asize, Asize, Asize);
		}
	}

	var HtmlString = "";

    for (var f in Foods) 
    {
		var i = f;
		f = Foods[f];

		HtmlString += "Food Amt " + i + ": " + f.Amt + "<br>";

		if (f.Amt <= 0)
		{
			if (verbose) console.log("in food delete");
			Grid[f.y0][f.x0] = undefined;
			Grid[f.y1][f.x1] = undefined;
			Grid[f.y2][f.x2] = undefined;
			Grid[f.y3][f.x3] = undefined;
			Foods.splice(i,1); 
			continue;
		}

		context.fillStyle = "red";

		context.fillRect(f.x0 * Asize, f.y0 * Asize, Fsize, Fsize);
    }


    document.getElementById('FoodAmt').innerHTML = HtmlString;

 //    context.fillStyle = "purple";

	// context.fillRect(homeX * Asize, homeY * Asize, Asize, Asize);
}

//checks wheather a move is legal or not
function isLegal(x, y)
{
	//console.log("x and y " + x + " " + y);
	
	if ( x < 0 || y < 0 || x >= GridSize || y >= GridSize)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function isFood(x, y)
{
	return ( Grid[y][x] instanceof Food );
}

function isTrail(x, y)
{
	return ( Grid[y][x] instanceof Trail );
}

//set update interval
function start()
{
	if (verbose) console.log("START");
	if (ID === undefined) ID = setInterval(update,100);
}

//stop generations
function stop()
{
	if (verbose) console.log("STOP");
	clearInterval(ID);
	ID = undefined;
}

//Creating a 2D Array function
function Create2DArray(rows) 
{
  var arr = [];

  for (var i = 0; i < rows; i++) 
  {
     arr[i] = [];
  }

  return arr;
}

function rgb(red, green, blue)
{
    var decColor =0x1000000 + blue + 0x100 * green + 0x10000 * red ;
    return '#'+decColor.toString(16).substr(1);
}


document.addEventListener
('keydown', function(event) 
{
	var moveX = 0;
	var moveY = 0;

	if (true)
	{
		if(event.keyCode == 32) 
		{
			//console.log('Space was pressed');
			if (ID)
			{
				stop();
			}
			else
			{
				start();
			}
		}
	}
});