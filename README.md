## YAY ANTS PROGRAM!

# How to initiate a git

1. Go to folder in terminal using cd
2. Initiate git with `git init`
3. Add remote server with `git remote add origin https://USERNAME@bitbucket.org/USERNAME/REPONAME.git`
4. Add new file using `git add FILENAME`
5. Commit added files using `git commit -a -m "ADD MESSAGE ABOUT COMMIT"`
6. When work is done push work to sever using `git push -u origin master`

# How to work with git basics
1. If there are any new files add them using `git add FILENAME`
2. Commit all files using `git commit -a -m "ADD MESSAGE ABOUT COMMIT"`
3. When work is done push work to sever using `git push`

[markdown basics](http://daringfireball.net/projects/markdown/basics)