window.onload = function () 
{
	init();
};

//canvas vars
var canvas;
var context;

//ant vars
var Ants = [];
var Asize = 5;
var Aamt = 100;
var moveX = 0;
var moveY = 0;

//Food var
var Foods = [];
var Fsize = Asize*2;
var Famt = 15;

//trail var
var Trails = [];
var Tsize = Asize;
var prevX;
var prevY;
var coloringIn;

//grid size
var GridSize;
var Grid;
var homeX;
var homeY;

//id for interval
var ID;

//bools, steps, golden
var verbose = true;
var steps = 0;
var DrawA = true;
var goldenRatio = (1 + Math.sqrt(5))/2;
var show = false;

//point
var point = 
{
	x : 0,
	y : 0
};

//ant class
function Ant( x, y, foundFood, foundTrail, steps, Trails, madeTrail, c )
{
	this.x = x;
	this.y = y;
	this.homeX = x;
	this.homeY = y;
	this.foundFood = foundFood;
	this.foundTrail = foundTrail;
	this.steps = steps;
	this.Trails = Trails;

	this.pathX = [];
	this.pathY = [];

	this.madeTrail = madeTrail;
	this.c = c;


	this.makePaths = function()
	{
		// console.log('making path');

		for (var i = 0; i < 10; i++)
		{
			moveX = Math.round(Math.random());
			switch(Math.round(Math.random()))
			{
				case 0: 
					moveX *= 1;
					break;
				case 1: 
					moveX *= -1;
					break;
			}

			moveY = Math.round(Math.random());
			switch(Math.round(Math.random()))
			{
				case 0: 
					moveY *= 1;
					break;
				case 1: 
					moveY *= -1;
					break;
			}

			// console.log('MX ' + moveX + ' ' + 'MY ' + moveY );

			this.pathX.push(moveX);
			this.pathY.push(moveY);
		}
	};

	this.flipPaths = function()
	{
		// console.log("FLIP");
		for (var path = 0; path < this.pathX.length; path++)
		{
			this.pathX[path] = -this.pathX[path];
			this.pathY[path] = -this.pathY[path];
		};
	};

	this.makePaths();
}

//trail class
function Trail(x, y, prevX, prevY, antIndex)
{
	this.x = x;
	this.y = y;
	this.prevX = prevX;
	this.prevY = prevY;
	this.antIndex = antIndex;
}

//food class
function Food( x0, y0, x1, y1, x2, y2, x3, y3, Amt)
{
	this.x0 = x0;
	this.y0 = y0;
	this.x1 = x1;
	this.y1 = y1;
	this.x2 = x2;
	this.y2 = y2;
	this.x3 = x3;
	this.y3 = y3;
	this.Amt = Amt;
}

//init function that reads file or displays deault
function init()
{
	console.log("init");

	canvas = document.getElementById("canvas");
	context = canvas.getContext("2d");

	GridSize = canvas.width/Asize;
	Grid = Create2DArray(GridSize);

	//if (verbose) console.log("Grid[0][0]: " + Grid[0][0] );

	// homeX = Math.round(GridSize/2); //(Math.random()*GridSize);
	// homeY = Math.round(GridSize/2); //(Math.random()*GridSize);

	if (verbose) console.log("GridSize: " + GridSize + "by" + GridSize );

	//add Ants to Antarray
	for (var i = 0; i < Aamt ; i++)
	{
		homeX = Math.round(Math.random()*GridSize);
		homeY = Math.round(Math.random()*GridSize);
		coloringIn = rgb( (Math.round(Math.random()*255)), 
						  (Math.round(Math.random()*255)), 
						  (Math.round(Math.random()*255)) 
						);
		// x, y, foundFood, foundTrail, steps, Trails, madeTrail, c
		Ants.push( new Ant( homeX, 
							homeY, 
							false, false, 0, [],false, coloringIn
						   ) 
				 );
	}

	//add food to grid
	// x0, y0, x1, y1, x2, y2, x3, y3, Amt
	for (var i = 0; i < Famt; i++)
	{
		var Fx = Math.round(Math.random()*(GridSize-2)), 
			Fy = Math.round(Math.random()*(GridSize-2)); 
		Foods.push( new Food( Fx, Fy, Fx + 1, Fy, Fx, Fy + 1, Fx + 1, Fy + 1, 100 ) );
	}
	
	//if (verbose) console.log("Food x y " + Foods[0].x + " " + Foods[0].y);
	
	for (var f in Foods) 
	{
		f = Foods[f];
		Grid[f.y0][f.x0] = f;
		Grid[f.y1][f.x1] = f;
		Grid[f.y2][f.x2] = f;
		Grid[f.y3][f.x3] = f;
	}

	//home
	if (verbose) console.log("home x y " + homeX + " " + homeY);

	//draw init screen
	draw();
	start();
}

//where ants make there move, foundFood booling changes, leave trail
function update()
{
	steps++;

	if (Foods.length === 0) stop();

	for(var antz in Ants)
	{
		var i = antz;

		antz = Ants[antz];

		// moveX = Math.round(Math.random());
		// switch(Math.round(Math.random()))
		// {
		// 	case 0: 
		// 		moveX *= 1;
		// 		break;
		// 	case 1: 
		// 		moveX *= -1;
		// 		break;
		// }

		// moveY = Math.round(Math.random());
		// switch(Math.round(Math.random()))
		// {
		// 	case 0: 
		// 		moveY *= 1;
		// 		break;
		// 	case 1: 
		// 		moveY *= -1;
		// 		break;
		// }

		// console.log('steps mod length ' + steps % antz.pathX.length);
		moveX = antz.pathX[steps % antz.pathX.length];
		moveY = antz.pathY[steps % antz.pathY.length];

		if (antz.foundTrail === true && !antz.foundFood)
		{
			//If on trail looking for food
			//if (verbose) console.log("followTrail");

			var spot = Grid[antz.y][antz.x];
			
			if ( spot instanceof Trail )
			{
				//follow trail
				antz.x = spot.prevX;
				antz.y = spot.prevY;
			}
			else if ( spot instanceof Food )
			{
				//found food go home
				//if (verbose) console.log("followTrail foundFood!!! " + spot.Amt);
				antz.foundFood = true;
				antz.foundTrail = false;
				spot.Amt--;	
			}
			else if ( spot === undefined )
			{
				//console.log("stuck");

			    for (var T in antz.Trails) 
			    {
					T = antz.Trails[T];
					// console.log( Grid[T.y][T.x] );
					
					if ( isNaN(T.x) || isNaN(T.y) )
					{
						continue;
					}
	
					if (Grid[T.y][T.x] != undefined)
						Grid[T.y][T.x] = undefined;
				}
				antz.Trails.splice(0, antz.Trails.length);
				antz.foundTrail = false;
				antz.madeTrail = false;
				antz.foundFood = false;

			}
		}
		else if ( isLegal( antz.x + moveX, antz.y + moveY) )
		{
			if (!antz.foundFood)
			{
				if ( isTrail( antz.x + moveX, antz.y + moveY ) )
				{
					//found trail now need to follow it
					//if (verbose)console.log("foundTrail");
					antz.x += moveX;
					antz.y += moveY;
					antz.foundTrail = true;
				}
				else if ( isFood( antz.x + moveX, antz.y + moveY ) )
				{
					//found food now start making trail home and take a piece of food
					antz.steps = steps - antz.steps;
					// if (verbose)console.log("Found Food in " + antz.steps +
					// 						 " steps.");
					antz.foundFood = true;
					prevX = antz.x;
					prevY = antz.y;
					antz.Trails.push(new Trail(antz.x, antz.y, antz.x + moveX, antz.y + moveY, i));
					Grid[antz.y][antz.x] = antz.Trails[antz.Trails.length-1];

					Grid[antz.y + moveY][antz.x + moveX].Amt--;
				}
				else
				{
					//if all else false make a pseudo-random move
					antz.x += moveX;
					antz.y += moveY;
				}
			}
			else if ( antz.foundFood && (antz.x != antz.homeX || antz.y != antz.homeY) )
			{
				//take shortest path home, maybe change this to follow unique trail home
				//if (verbose)console.log("ET phone home");
				var slopeX = antz.homeX - antz.x;
				var slopeY = antz.homeY - antz.y;
				prevX = antz.x;
				prevY = antz.y;

				if (slopeX !== 0) antz.x += (slopeX > 0 ? 1 : -1);
				if (slopeY !== 0) antz.y += (slopeY > 0 ? 1 : -1);
				
				if(show)console.log('madeTrail ' + antz.madeTrail +
									' Grid[x][y] ' + Grid[antz.y][antz.x])

				if (!antz.madeTrail && (Grid[antz.y][antz.x] === undefined) )
				{
					antz.Trails.push(new Trail(antz.x, antz.y, prevX, prevY, i));
					Grid[antz.y][antz.x] = antz.Trails[antz.Trails.length-1];
				}
			}
			else
			{
				//home now follow food again
				//if (verbose) console.log("Winner " + i + " took " + antz.steps + " ant steps and world steps " + steps);
				antz.foundFood = false;
				antz.foundTrail = false;
				antz.madeTrail = true;

			}
		}
		else
		{
			// console.log( 'wall' );
			antz.flipPaths();
		}
	}
	draw();
}

//draws function for "animation"
function draw()
{
	//console.log("DRAW");
	context.clearRect(0, 0, canvas.width, canvas.height);

	// Draw trails and ants
	for (var antz in Ants) 
	{
		antz = Ants[antz];

		for (var t in antz.Trails) 
		{
			//if (Ants[Trails[t].antIndex].foundFood == false) {Trails.splice(t,1); continue;};
			t = antz.Trails[t];

			// console.log(antz.color);

			context.fillStyle = antz.c != undefined ? antz.c : null;

			// if (verbose)console.log( antz.c );

			context.fillRect(t.x * Tsize, t.y * Tsize, Tsize, Tsize);
		}

		if(DrawA)
		{
			context.fillStyle = "black";

			context.fillRect(antz.x * Asize, antz.y * Asize, Asize, Asize);
		}
	}

	var HtmlString = "";

	for (var f in Foods) 
	{
		var i = f;
		f = Foods[f];

		if (f.Amt <= 0)
		{
			if (verbose) console.log("in food delete");
			Grid[f.y0][f.x0] = undefined;
			Grid[f.y1][f.x1] = undefined;
			Grid[f.y2][f.x2] = undefined;
			Grid[f.y3][f.x3] = undefined;
			Foods.splice(i,1); 
			continue;
		}

		HtmlString += "Food Amt " + i + ": " + f.Amt + "\t";

		context.fillStyle = "red";

		context.fillRect(f.x0 * Asize, f.y0 * Asize, Fsize, Fsize);
	}
	
	HtmlString += "<br>Step: " + steps;

	document.getElementById('FoodAmt').innerHTML = HtmlString;

 //	context.fillStyle = "purple";

	// context.fillRect(homeX * Asize, homeY * Asize, Asize, Asize);
}

//checks wheather a move is legal or not
function isLegal(x, y)
{
	//console.log("x and y " + x + " " + y);
	
	if ( x < 0 || y < 0 || x >= GridSize || y >= GridSize)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function isFood(x, y)
{
	if ( isNaN(x) || isNaN(y) )
	{
		return false;
	}

	if ( Grid[y][x] instanceof Food )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function isTrail(x, y)
{
	if ( isNaN(x) || isNaN(y) )
	{
		return false;
	}

	if ( Grid[y][x] instanceof Trail )
	{
		return true;
	}
	else
	{
		return false;
	}
}

//set update interval
function start()
{
	console.log("START");
	if (ID == undefined) ID = setInterval(update,10);
}

//stop generations
function stop()
{
	console.log("STOP");
	clearInterval(ID);
	ID = undefined;
}

//Creating a 2D Array function
function Create2DArray(rows) 
{
  var arr = [];

  for (var i = 0; i < rows; i++) 
  {
	arr[i] = [];
  }

  return arr;
}

function rgb(red, green, blue)
{
    var decColor =0x1000000 + blue + 0x100 * green + 0x10000 * red ;
    return '#'+decColor.toString(16).substr(1);
}


document.addEventListener
('keydown', function(event) 
{
	if (true)
	{
		if(event.keyCode == 32) 
		{
			//console.log('Space was pressed');
			if (ID)
			{
				stop();
			}
			else
			{
				start();
			}
		}
	}

	if ( isLegal(Ants[0].x + moveX, Ants[0].y + moveY) )
	{
		Ants[0].x += moveX;
		Ants[0].y += moveY;
	}
	else if ( isFood( Ants[0].x + moveX, Ants[0].y + moveY ) )
	{
		console.log("isFood");
	}
});